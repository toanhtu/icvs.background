export const MAX_ACURRACY_NUMBER_MATCHING_TITLE = 0.75;
export const MAX_ACURRACY_NUMBER_MATCHING_TITLE_AND_INDUSTRY = 0.75;
export const status = {
    NOT_WORKING: 'NOT_WORKING',
    ACC_NOT_WORKING: 'ACC_NOT_WORKING',
    GENERATED: 'GENERATED',
    REQUESTED: 'REQUESTED',
    REACH_LIMITED: 'REACH_LIMITED'
};
export var GAMING_CONNECT = [
    'activity',
    'faceted_search'
];
export const GAMING_CONNECT_STATUS = {
    ACTIVITY: 'activity',
    FACETED_SEARCH: 'faceted_search',
    DIRECT_MESSAGING: 'directing_messaging'
};
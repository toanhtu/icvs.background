import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";
const {Schema} = mongoose;

const LogActivitySchema = Schema({
    created_by: String,
    type: String,
    meta: Object,
    created_at: {type: Date, default: moment.now()},
    updated_at: {type: Date, default: moment.now()}
});
const LogActivityModel = mongoose.model("projects", LogActivitySchema, 'projects');
export const KANBAN_ADD_NEW = 'KANBAN_ADD_NEW';
export default LogActivityModel;

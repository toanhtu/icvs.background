import mongoose from 'mongoose';
import moment from "moment";

const Schema = mongoose.Schema;

const ExtLinkedinUpcommingConnectSchema = mongoose.Schema({

    requesterNiid: {type: Object},
    profile:{type: Object},
    upcomming_time: {type: Date},
    activity_id: {type: String},
    updated_at: {type: Date, default:moment.now()},
    created_at: {type: Date, default:moment.now()},
    status:{type: Object},
    facke: {type: Boolean}

});


const ExtLinkedinUpcommingConnectModel = mongoose.model('ext_linkedin_upcomming_connect', ExtLinkedinUpcommingConnectSchema, 'ext_linkedin_upcomming_connect')

/**
 *
 * @param niid
 * @param data
 */
export async function updateUpCommingConnect(niid, data) {
    let upcomming = await ExtLinkedinUpcommingConnectModel.findOne({requesterNiid: niid})
    if (upcomming){
        await ExtLinkedinUpcommingConnectModel.updateOne({requesterNiid: niid}, data)
        console.log("thanh cong");
    }
}

/**
 *
 * @param job_title
 */
export async function getUpCommingConnect(job_title) {
    return await ExtLinkedinUpcommingConnectModel.find({
        'status.required_target_meta.profileror_meta.job_title': {
            $in: job_title,
        }
    })
        .sort({
            _id: -1
        }).limit(100)
        .exec()
        .then((UpcommingData) => {
            return UpcommingData;
        }).catch((err) => {
            console.log(err)
        })
}

/**
 *  lay ra requesterNiid theo niid
 * @param niid
 */

export  async function getLinkedinUpcomingData(niid) {
    return  await ExtLinkedinUpcommingConnectModel.findOne({
        niid : niid
    })
        .sort({
            _id: -1
        }).limit(100)

}


export default ExtLinkedinUpcommingConnectModel;


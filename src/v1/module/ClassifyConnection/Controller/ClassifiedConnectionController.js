import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';
import moment from 'moment';

import _ from 'lodash';

import {
    CLASSIFIED,
    updateLinkedinProfilesRelationship,
    updateStatusLinkedinProfilesRelationship,
    getLinkedinProfileRelationshipForClassified,
    getLinkedinProfileRelationship
} from '../../../model/LinkedinProfilesRelationshipModel';

import {
    getLinkedinClassifyRequest, SUCCESS, updateLinkedinClassifyRequest
} from '../../../model/ExtLinkedinClassifyRequestModel';
import {
    getClassifiedTargetClassified,
    getTargetToClassified,
    markClassifiedTarget, updatePausedClassifiedTarget
} from '../../../model/ExtLinkedinTargetModel';

import {makeMatchingTargetProfile} from "./../../Common/ProfileDataHelper";
import {step_verify_profile} from "../../Common/TargetHelper";
import {parseDate} from "../../Common/DateTimeHelper";
import {getProfileData} from "../../../model/LinkedinProfilesModel";


const main = async () => {

    try {
        console.log('begin');
        let linkedinClassifyRequests = await getLinkedinClassifyRequest();

        if (linkedinClassifyRequests.length === 0) {
            console.log('end process');
            process.exit(1);
        }

        let linkedinClassifyRequestMap = _.map(linkedinClassifyRequests, async (classifyRequest) => {

            let niid = _.get(classifyRequest, 'niid');
            let max_num_connection = _.get(classifyRequest, 'num_connections');

            let max_linkedin_relationship = await getLinkedinProfileRelationship(niid);

            let targets = await getTargetToClassified(niid, Math.min(max_num_connection, max_linkedin_relationship));
            if (targets.length === 0) {

                let allTargets = await getClassifiedTargetClassified(niid);
                let min_last_classified_amount = _.get(allTargets, '0.last_classified_amount');

                if (min_last_classified_amount >= max_num_connection) {
                    await updateLinkedinClassifyRequest(classifyRequest.id, {
                        status: SUCCESS,
                        re_sync: false
                    });
                }

                let targetNoPause = _.filter(allTargets, (target) => {
                    return !target.paused_at;
                });

                if (targetNoPause.length === 0) {
                    await updateLinkedinClassifyRequest(classifyRequest.id, {
                        status: SUCCESS,
                        re_sync: false
                    });

                }

                if (min_last_classified_amount >= max_linkedin_relationship) {
                    await updateLinkedinClassifyRequest(classifyRequest.id, {classified_at: moment().toDate()});
                }
            }

            /**
             * Match các đối tượng với profile đó
             */
            let targetsMap = _.map(targets, async (target) => {
                let last_classified_amount = _.get(target, 'last_classified_amount', 0);

                let requestForClassifieds = await getLinkedinProfileRelationshipForClassified({
                    niid,
                    last_classified_amount,
                });

                if (_.isEmpty(requestForClassifieds)) {

                    let pausedTarget = await updatePausedClassifiedTarget({
                        niid: niid,
                        last_classified_amount: {
                            '$gte': max_linkedin_relationship
                        }
                    });
                    await Promise.all([pausedTarget]).then(() => {
                        console.log('endloop');

                    })
                }


                let requestForClassifiedsMap = _.map(requestForClassifieds, async (requestForClassified) => {
                    let id = requestForClassified._id;
                    let niid = _.get(requestForClassified, 'niid');
                    let profile = _.get(requestForClassified, 'profile_meta');

                    if (_.isEmpty(profile)) {
                        let profileData = await getProfileData(niid);

                        if (_.isEmpty(profileData)) {
                            let emptyProfile = await updateStatusLinkedinProfilesRelationship(id);
                            if(emptyProfile) {
                                console.log('end process', niid);
                                process.exit(1)
                            }
                        }

                        profile = makeMatchingTargetProfile(profileData);
                    }

                    let match = await step_verify_profile(profile, target);
                    let target_ids = _.get(requestForClassified, 'target_ids', []);


                    let dataUpdate = {
                        ...match ? {
                            target_ids: _.uniq([...target_ids, target._id.toString()])
                        } : {target_ids: _.uniq(target_ids)},
                        profile_meta: profile,
                        status: CLASSIFIED,
                        add_to_project: false,
                        updated_at: moment.now()
                    };

                    return await updateLinkedinProfilesRelationship(id, dataUpdate);
                });

                let data = {
                    last_classified_niid: niid,
                    last_classified_amount: requestForClassifieds.length,
                };


                await Promise.all(
                    requestForClassifiedsMap,
                ).then(async()=> {
                    let markTarget = await markClassifiedTarget(target, data);
                })


            });
            await Promise.all(
                targetsMap
            )
        });

        await Promise.all(
            linkedinClassifyRequestMap
        ).then(() => {
            console.log('endloop');
            process.exit(1);
        })

    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }
};

main();



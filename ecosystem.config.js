module.exports = {
    apps: [
        {
            name: "classified_connection",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassifyConnection/Controller/ClassifiedConnectionController.js",
        },
        {
            name: "classified_target",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassifyConnection/Controller/ClassifiedConnectionByTargetController.js",
        },
        {
            name: "classified_marked",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassifyConnection/Controller/ClassifiedChecked.js",
        },
        {
            name: "add_to_project",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassifyConnection/Controller/AddToProject.js",
        },
        {
            name: "check_success",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassifyConnection/Controller/CheckedSuccessRequest.js",
        },
        {
            name: "transform",
            script: "./node_modules/.bin/babel-node ./src/v1/module/Transform/Controller/LinkedinTransformController.js"
        },
        {
            name: "auto_gen_connect",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassGenUpcommingConnect/Controller/AutoGenConnectionController.js",
        },{
            name: " transform",
            script: "./node_modules/.bin/babel-node ./src/v1/module/Transform/Controller/LinkedinTransformController.js",

        }, {
            name: "normalized_profile",
            script: "./node_modules/.bin/babel-node ./src/v1/module/ClassifyConnection/Controller/NormalizeProfileDataController.js",
        },
        {
            name: "index_linkedin_profile",
            script: "./node_modules/.bin/babel-node ./src/v1/module/LinkedinProfile/IndexElasticsearchController.js",
        }
    ]
}



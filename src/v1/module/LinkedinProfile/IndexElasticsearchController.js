import dotenv from 'dotenv/config';
import openConnection from '../../database/database';
import {documentAdd} from '../../database/elasticsearch';
import moment from 'moment';
import _ from 'lodash';
import {
    getBackgroundBatch,
    INDEX_LINKEDIN_PROFILE,
    markBackgroundBatch,
    SYNC_LINKEDIN_PROFILE
} from "../../model/BackgroundBatchModel";

import LinkedinProfileTransformedModel from "../../model/LinkedinProfileTransformedModel";
import {PROCESSING} from "../../model/ExtLinkedinClassifyRequestModel";

const main = async () => {
    try {

        let backgroundBatch = await getBackgroundBatch(INDEX_LINKEDIN_PROFILE);
        let limit = 200;
        let skip = 0;

        if (!_.isEmpty(backgroundBatch)) {
            console.log('run by background batch');
            limit = _.get(backgroundBatch, 'limit');
            skip = _.get(backgroundBatch, 'skip');
        }

        let linkedinProfileTransformedArray = await LinkedinProfileTransformedModel
            .find({
                // $or: [
                //     {
                //         synced_at: {
                //             $exists: false
                //         }
                //     }, {
                //         synced_at: {
                //             '$lt': moment().subtract(15, 'days')
                //         }
                //     }
                // ]
            })
            .sort({
                _id: 1
            })
            .skip(skip)
            .limit(limit)
            .exec()
            .then(
                function (result, err) {
                    if (err) {
                        console.log(err);
                        process.exit(1);
                    }

                    return result;
                });

        if (linkedinProfileTransformedArray.length) {
            let markedID = await markBackgroundBatch({
                limit: limit,
                skip: skip + limit,
                status: PROCESSING,
                type: INDEX_LINKEDIN_PROFILE
            });
        }

        let linkedinProfileMap = linkedinProfileTransformedArray.map(async (item) => {
            let niid = _.get(item, 'niid');

            let linkedinProfile = await documentAdd(niid, _.omit(item._doc, ['_id', 'profile_meta']));
            console.log(linkedinProfile, 'linkedinProfile');
                type: SYNC_LINKEDIN_PROFILE
            });


        await Promise.all(linkedinProfileMap).then(() => {
            console.log('done');
            process.exit(0);
        })

    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }
};

main();

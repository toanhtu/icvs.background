import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';
import moment from 'moment';
import _ from 'lodash';
import {getUserSettingData} from "../../../model/UserSettingModel";
import {getLinkedinToken} from "../../../model/ExtLinkedinTokenModel";
import {getJobTitleTarget} from "../../../model/ExtLinkedinTargetModel";
import {
    getLinkedinUpcomingData,
    getUpCommingConnect,
    updateUpCommingConnect
} from "../../../model/ExtLinkedinUpcommingConnectModel";
import {getBackgroundBatch, markBackgroundBatch, PROCESSED, SYNC_GEN_AUTO} from "../../../model/BackgroundBatchModel";
import {geLinkedinProfileRelationShipAutoGenConnect} from "../../../model/LinkedinProfilesRelationshipModel";

const main = async () => {

    try {
        let backgroudGenAuto = await getBackgroundBatch(SYNC_GEN_AUTO)
        let skip = 0;
        let limit = 50;
        if (!_.isEmpty(backgroudGenAuto)) {
            limit = _.get(backgroudGenAuto, 'limit');
            skip = _.get(backgroudGenAuto, 'skip');
        }

        let user_setting_data = await getUserSettingData(skip, limit);
        if (user_setting_data.length) {
            let dbskip = {
                limit: limit,
                skip: skip + limit,
                status: PROCESSED,
                type: SYNC_GEN_AUTO
            }
            let saveSkip = markBackgroundBatch(dbskip);
        }
        let a = _.map(user_setting_data, async (user_setting) => {
            let user_id = _.get(user_setting, "user_id", null)

            console.log("user_id: " + user_id);
            let linkedinToken = await getLinkedinToken(user_id);
            _.each(linkedinToken, async (linkedin_token_data) => {
                let niid_token = _.get(linkedin_token_data, "niid", null);

                console.log(niid_token)

                // check niid đã gửi yêu cầu vào trong upcoming hay chưa
                let linkedinUpcomingData = await getLinkedinUpcomingData(niid_token)
                _.each(linkedinUpcomingData, async (linkedin_up_coming) => {
                    let requesterNiid = _.get(linkedin_up_coming, "requesterNiid", null);

                    // không có requesterNiid chưa từng gửi yêu cầu kết nối
                    if (requesterNiid == null) {
                        console.log("chưa gửi kết nối " + niid_token)


                        // check niid trong relationship  đã tồn tại hay chưa
                        let linkedinProfilesRelationshipData = geLinkedinProfileRelationShipAutoGenConnect(niid_token);
                        _.each(linkedinProfilesRelationshipData, async (linkedin_relationship) => {
                            let friendNiid = _.get(linkedin_relationship, "friendNiid", null)


                            // nếu trong relationship không có niid và friendNiid không phải là bạn
                            if (friendNiid == null && niid_token == nul) {

                                console.log(niid_token + " chưa phải là bạn bè ")

                                let linkedTargetData = await getJobTitleTarget(niid_token);
                                _.each(linkedTargetData, async (target_data) => {
                                    let job_title = _.get(target_data, "job_title", null)
                                    let upComingData = await getUpCommingConnect(job_title)

                                    console.log(job_title + "job_title");

                                    _.each(upComingData, async (up_coming_connec_db) => {
                                        let Profile = _.get(up_coming_connec_db, "profile", null);
                                        let Upcoming_time = moment.now();
                                        let Activity_id = -1
                                        let fake = true;
                                        let data = {
                                            profile: Profile,
                                            upcomming_time: Upcoming_time,
                                            activity_id: Activity_id,
                                            fake: fake
                                        };
                                        console.log(data + "data");
                                        await updateUpCommingConnect(niid_token, data);
                                    })
                                })
                            } else if (friendNiid !== null && niid_token !== null) {
                                console.log("tài khoản" + friendNiid + " đã là bạn bè ")
                            }
                        })
                    } else if (requesterNiid != null) {
                        console.log("tài khoản " + requesterNiid + "đã từng gửi yêu cầu kết nối")
                    }
                })


            })
        })
        await Promise.all(a).then(() => {
            console.log("end process")
            process.exit(0);
        })
    } catch (e) {
        if (e) {
            console.log(e)
        }
    }
}
main();
import mongoose from 'mongoose';
import moment from 'moment';

const Schema = mongoose.Schema;
const LinkedinProfileTransformedSchema = Schema({
    niid: String,
    publicIdentifier: String,
    firstName: String,
    lastName: String,
    headline: String,
    summary: String,
    locationName: String,
    address: String,
    pictureUrl: String,
    educations: Array,
    positions: Array,
    skills: Array,
    fullName: String,
    profile_meta: Object,
    synced_at: Date,
    transformed_at: {type: Date, default: moment.now()},
    created_at: {type: Date, default: moment.now()},
    updated_at: {type: Date, default: moment.now()}
});

const LinkedinProfileTransformedModel = mongoose.model("linkedin_profile_transformed", LinkedinProfileTransformedSchema, 'linkedin_profile_transformed');

export default LinkedinProfileTransformedModel;


export async function updateOrCreateLinkedinTransformed(niid, dataTransformed) {
    try {
        await LinkedinProfileTransformedModel.findOne({niid: niid}, async function (err, transformed) {
                if (err) {
                    return;
                }
                try {
                    if (transformed) {
                        await LinkedinProfileTransformedModel.updateOne({niid: niid}, dataTransformed);
                    } else {
                        console.log(dataTransformed,'vao day ');
                       let profileTransformed =  await LinkedinProfileTransformedModel.create(dataTransformed, function (err, transformed) {
                            if (err) {
                                console.log('save loi');
                                return;
                            }
                            return transformed;
                        });
                       console.log(profileTransformed);
                    }
                }catch(err) {
                    console.log(err);
                    return false;
                }
            }
        );
        return true;
    } catch
        (err) {
        console.log(err);
        return false;
    }
}
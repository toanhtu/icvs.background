import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';
import moment from 'moment';

import _ from 'lodash';

import {
    CLASSIFIED,
    updateLinkedinProfilesRelationship,
    updateStatusLinkedinProfilesRelationship,
    getLinkedinProfileRelationshipForClassified,
    getLinkedinProfileRelationship
} from '../../../model/LinkedinProfilesRelationshipModel';

import {
    getLinkedinClassifyRequest, SUCCESS, updateLinkedinClassifyRequest
} from '../../../model/ExtLinkedinClassifyRequestModel';
import {
    getClassifiedTargetClassified, getTargetClassified,
    getTargetToClassified,
    markClassifiedTarget, updatePausedClassifiedTarget
} from '../../../model/ExtLinkedinTargetModel';

import {makeMatchingTargetProfile} from "./../../Common/ProfileDataHelper";
import {step_verify_profile} from "../../Common/TargetHelper";
import {parseDate} from "../../Common/DateTimeHelper";
import {getProfileData} from "../../../model/LinkedinProfilesModel";


const main = async () => {

    try {
        let targets = await getTargetClassified();
        if (targets.length === 0) {
            console.log('end process');
            process.exit(0);
        }
        await Promise.all(
            _.map(targets, async (target) => {
                let last_classified_amount = _.get(target, 'last_classified_amount', 0);
                let niid = _.get(target, 'niid');
                let requestForClassifieds = await getLinkedinProfileRelationshipForClassified({
                    niid,
                    last_classified_amount,
                });
                if (_.isEmpty(requestForClassifieds)) {
                    process.exit(1);
                }
            })
        ).then(() => {
                console.log('endloop');
                process.exit(1);
            })


    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }

};

main();



import mongoose from 'mongoose';
const MONGO_DB_HOST = process.env.MONGO_DB_HOST || 'services_icvs_mongo_1';
const MONGO_DB_PORT = process.env.MONGO_DB_PORT || '27017';
const MONGO_DB_DATABASE = process.env.MONGO_DB_DATABASE || 'icvs';
// const MONGO_DB_USERNAME = process.env.MONGO_DB_USERNAME || 'icvs';
// const MONGO_DB_PASSWORD = process.env.MONGO_DB_PASSWORD || 'icvs@123';

const url = `mongodb://${MONGO_DB_HOST}:${MONGO_DB_PORT}/${MONGO_DB_DATABASE}?authSource=admin`;

const openConnection = mongoose.connect(url, {
    // user: MONGO_DB_USERNAME,
    // pass: MONGO_DB_PASSWORD,
    useNewUrlParser: true,
    useUnifiedTopology: true
})
    .then(() => console.log("Mongodb connected"))
    .catch(err => console.log(err));
mongoose.set('useFindAndModify', false);
mongoose.connection.on("error", function(err) {
    console.log('Failed to connect to DB');
});

mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection to DB disconnected');
});

const gracefulExit = function() {
    mongoose.connection.close(function () {
        console.log('vao day');
        process.exit(0);
    });
};

process.on('exit', gracefulExit);

export default openConnection;

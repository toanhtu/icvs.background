import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";

const Schema = mongoose.Schema;

const LinkedinProfilesSchema = Schema({
    niid: String,
    publicIdentifier: String,
    firstName: String,
    lastName: String,
    headline: String,
    summary: String,
    locationName: String,
    address: String,
    pictureUrl: String,
    educations: Array,
    positions: Array,
    skills: Array,
    fullName: String,
    profile_meta: Object,
    transformed_at: {type: Date, default: moment.now()},
    created_at: {type: Date, default: moment.now()},
    updated_at: {type: Date, default: moment.now()}


});



const LinkedinTransformedModel = mongoose.model("linkedin_profile_transformed", LinkedinProfilesSchema, 'linkedin_profile_transformed');





export async function insertLinkedinTransformed( niid, dataTransformed) {
    try {
        let transformed = await LinkedinTransformedModel.findOne({niid: niid})
        if (transformed) {
            await LinkedinTransformedModel.updateOne({niid: niid}, dataTransformed)
            console.log("thanh cong")
        } else {

            let profileTransformed = new LinkedinTransformedModel(dataTransformed);
            await profileTransformed.save();
            console.log(" thanh cong")

        }

        return true;
    }catch (e) {
        if (e){
            console.log('error', e)
            return false
        }
    }
}

export  default LinkedinTransformedModel;
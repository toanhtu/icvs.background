import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const ExtLinkedinTokenSchema = mongoose.Schema({
    _id: Object,
    niid: String,
    user_id: String
})


const ExtLinkedinTokenModel = mongoose.model('ext_linkedin_token', ExtLinkedinTokenSchema, 'ext_linkedin_token')

export default ExtLinkedinTokenModel;


/**
 * lấy ra niid theo user_id
 * @param user_id
 */
export async function getLinkedinToken(user_id) {
    return await ExtLinkedinTokenModel.findOne({
        user_id: user_id
    })
        .sort({
            _id: -1
        })
        .limit(100)
}
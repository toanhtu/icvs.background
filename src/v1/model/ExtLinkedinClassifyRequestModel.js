import mongoose from 'mongoose';
import moment from "moment";
import LinkedinProfilesRelationshipModel from "./LinkedinProfilesRelationshipModel";

const Schema = mongoose.Schema;

const ExtLinkedinClassifyRequestSchema = Schema({
    niid: String,
    status: String,
    real: Boolean,
    num_connections: Number,
    last_num_connections: Number,
    re_sync: Boolean,
    classified_at: Date,
    classified_checked_at: Date,
    check_success_at: Date
});
export const PENDING = 'PENDING';
export const PROCESSING = 'PROCESSING';
export const SUCCESS = 'SUCCESS';
export const SUCCESS_SYNC = 'SUCCESS_SYNC';
const ExtLinkedinClassifyRequestModel = mongoose.model("ext_linkedin_classify_request", ExtLinkedinClassifyRequestSchema, 'ext_linkedin_classify_request');

export default ExtLinkedinClassifyRequestModel;


export async function getLinkedinClassifyRequest() {

    return await ExtLinkedinClassifyRequestModel.find({
        $and: [
            {
                $or: [{
                    status: PROCESSING,
                    real: true,
                }, {
                    re_sync: true
                }]
            },
            {
                $or: [
                    {
                        classified_at: {
                            '$exists': false
                        }
                    },
                    {
                        classified_at: {
                            $lte: moment().subtract(1, 'hours').toDate()
                        }
                    }
                ]
            }
        ]
    })
        .limit(300)
        .exec()
        .then((data) => {
            return data;
        })
        .catch((err) => {
            return 'error occured ' + JSON.stringify(err);
        });
}

export async function updateLinkedinClassifyRequest(id, data) {
    try {
        await ExtLinkedinClassifyRequestModel.updateOne({_id: new mongoose.Types.ObjectId(id)}, {$set: data}, {});

        return true;

    } catch (error) {
        process.exit(1);
        return false;
    }
}



/**
 * Lấy ra profile đã đồng bộ data và chờ để phân loại
 */
export async function getRequestClassified() {

    return await ExtLinkedinClassifyRequestModel.find({
        real: true,
        status: {
            '$in': [PROCESSING, SUCCESS]
        },
        '$or': [
            {
                classified_checked_at: {
                    '$exists': false
                }
            },
            {
                classified_checked_at: {
                    $lte: moment().subtract(1, 'minutes').utcOffset('+0700').format("YYYY-MM-DDTHH:mm:ss.SSSZ")
                }
            }
        ]
    })
        .sort({
            _id: 1
        })
        .limit(300)
        .exec()
        .then((profile) => {
            return profile;
        })
        .catch((err) => {
            console.log(err);
            return 'error occured' + JSON.stringify(err);
        });
}
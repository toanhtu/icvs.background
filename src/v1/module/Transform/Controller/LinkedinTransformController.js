import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';
import moment from 'moment';

import _ from 'lodash';
import LinkedinProfilesModel, {getlinkedProfileData} from "../../../model/LinkedinProfilesModel";
import {transformData} from "../../Common/ProfileDataHelper";
import LinkedinTransformedModel from "../../../model/LinkedinTransformedModel";
import {
    getBackgroundBatch,
    markBackgroundBatch,
    SYNC_LINKEDIN_PROFILE,
    PROCESSING, PROCESSED
} from "../../../model/BackgroundBatchModel";
import {insertLinkedinTransformed} from "../../../model/LinkedinTransformedModel";

const main = async () => {
    try {
        let backgroundBatch = await getBackgroundBatch(SYNC_LINKEDIN_PROFILE);
        let limit = 200;
        let skip = 0;

        if (!_.isEmpty(backgroundBatch)) {
            limit = _.get(backgroundBatch, 'limit');
            skip = _.get(backgroundBatch, 'skip');
        }

        let linkedinProfiles = await getlinkedProfileData(skip, limit)
        if (linkedinProfiles.length) {
            let dataskip = {
                limit: limit,
                skip: skip + limit,
                status:PROCESSED,
                type: SYNC_LINKEDIN_PROFILE
            }
            let markedID = await markBackgroundBatch(dataskip);
        }

        let linkedinProfileMap = _.map(linkedinProfiles, async (linkedinProfile) => {
            let niid = _.get(linkedinProfile, 'niid');
            let isDuplicate = await checkDuplicateNiid(niid);

            if (isDuplicate) {
                return null;
            }

            let publicIdentifier = _.get(linkedinProfile, 'publicIdentifier');
            let profileData = _.get(linkedinProfile, 'profileData');
            let included = _.get(profileData, 'included');
            if (!_.isEmpty(included)) {
                profileData = included;
            }

            let dataTransformed = transformData(profileData, niid);
            if (!dataTransformed) {
                console.log(niid, 'not profileData');
                return;
            }
            await insertLinkedinTransformed(niid, dataTransformed)
        });

        await Promise.all(linkedinProfileMap).then(() => {
            console.log('done');
            process.exit(0);
        })
        await Promise.all(linkedinProfileMap).then(() => {
            console.log("abcxyz")
            process.exit(0);
        })
    } catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }
};

main();

const checkDuplicateNiid = async (niid) => {
    let linkedinTransformed = await LinkedinTransformedModel.findOne({
            niid: niid,
            transformed_at: {
                '$gte': moment().subtract(15, 'days')
            }
        }
    );

    return !_.isEmpty(linkedinTransformed);
};


import elasticsearch from "elasticsearch";
const ELASTIC_URL = process.env.ELASTIC_HOST || 'localhost:9220';

const ElasticSearchClient = new elasticsearch.Client({
    host: ELASTIC_URL,
    log: 'error'
});


export const documentAdd = async function (id, data, callback) {
    return await ElasticSearchClient.index({
        index: 'linkedin',
        id: id,
        body: data
    }, function (err, response , status) {
        console.log(response);
        if(callback) {
            callback(response);
        }
    });
};

export default ElasticSearchClient;

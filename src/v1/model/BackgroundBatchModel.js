import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";
const {Schema} = mongoose;

const BackgroundBatchSchema = Schema({
    type: String,
    skip: Number,
    limit: Number,
    status: String,
    id_start: String,
    last_processing_id: String,
    processed_at: Date,
    created_at: {type: Date, default: moment.now()},
    updated_at: {type: Date, default: moment.now()}
});
const BackgroundBatchModel = mongoose.model("job_batch_tracking", BackgroundBatchSchema, 'job_batch_tracking');

export default BackgroundBatchModel;
export const SYNC_LINKEDIN_PROFILE = "SYNC_LINKEDIN_PROFILE";
export const SYNC_GEN_AUTO = "SYNC_GEN_AUTO"
export const PROCESSING ="PROCESSING";
export const PROCESSED ="PROCESSED";


export async function markBackgroundBatch(data) {
    let updateResult = await BackgroundBatchModel.create(
        data,
        function (err, batch) {
            if (err) {
                console.log('error', err);
                return false;
            }

             return _.get(batch, '_id')
         });

    console.log(updateResult);
     return true;
}

export async function getBackgroundBatch(type) {
    return BackgroundBatchModel.findOne({
        type: type
    }, null, {
        sort: {_id: -1}
    });
}
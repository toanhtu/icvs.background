import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';

import _ from 'lodash';
import moment from 'moment';

import {
    markLinkedinProfilesRelationship
} from '../../../model/LinkedinProfilesRelationshipModel';

import {
    getRequestClassified
} from '../../../model/ExtLinkedinClassifyRequestModel';
import {
    getMinTargetClassified,
} from '../../../model/ExtLinkedinTargetModel';
import ExtLinkedinClassifyRequestModel from "../../../model/ExtLinkedinClassifyRequestModel";


const main = async () => {

    try {
        let requestClassifieds = await getRequestClassified();

        let requestClassifiedsMap = _.map(requestClassifieds, async (requestClassified) => {
            let friendNiid = _.get(requestClassified, 'niid');
            let num_connections = _.get(requestClassified, 'num_connections');
            let min_classified_target = await getMinTargetClassified(friendNiid);
            console.log(friendNiid, min_classified_target);

            await markLinkedinProfilesRelationship({
                friendNiid, min_classified: Math.min(min_classified_target, num_connections)
            });

            let updated = await ExtLinkedinClassifyRequestModel.findByIdAndUpdate(requestClassified._id, {
                classified_checked_at: moment.now()
            },function (err, result) {
                if (err){
                    console.log(err);
                    return false;
                }
                return result
            });
            console.log(updated,'updated');
        });
        await Promise.all(requestClassifiedsMap).then(() => {
            process.exit(0);
        })
    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }

};

main();



import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';

import _ from 'lodash';
import moment from 'moment';

import {
    getLinkdedinRelationShipToAddProject,
} from '../../../model/LinkedinProfilesRelationshipModel';

import ExtLinkedinTargetModel from "../../../model/ExtLinkedinTargetModel";
import LinkedinProfilesModel from "../../../model/LinkedinProfilesModel";
import KanbanModel from "../../../model/KanbanModel";
import ProjectStatusModel from "../../../model/ProjectStatusModel";
import LogActivityModel, {KANBAN_ADD_NEW} from "../../../model/LogActivityModel";
import LinkedinProfilesRelationshipModel from "../../../model/LinkedinProfilesRelationshipModel";
import ExtLinkedinClassifyRequestModel, {SUCCESS_SYNC} from "../../../model/ExtLinkedinClassifyRequestModel";


const main = async () => {

    try {
        let linkedinRelationships = await getLinkdedinRelationShipToAddProject();

        let linkedinRelationshipsMap = _.map(linkedinRelationships, async (linkedinRelationship) => {
            let niid = _.get(linkedinRelationship, 'niid');
            let linkedin_relationship_id = _.get(linkedinRelationship, '_id');
            let friendNiid = _.get(linkedinRelationship, 'friendNiid');
            let target_ids = _.get(linkedinRelationship, 'target_ids', []);

            if (target_ids.length > 0) {
                let targetMap = _.map(target_ids, async (target_id) => {

                    let targetModel = await ExtLinkedinTargetModel.findById(target_id, function (err, target) {
                        if (err) return handleError(err);
                        return target;
                    });
                    let project_id = _.get(targetModel, 'project_id');
                    let user_id = _.get(targetModel, 'created_by');
                    let added = await addToProject({user_id, niid, project_id, linkedin_relationship_id});
                });
                await Promise.all(targetMap).then(async () => {
                   await LinkedinProfilesRelationshipModel.findByIdAndUpdate(linkedin_relationship_id, {
                        add_to_project: true,
                        sync: true,
                        updated_at: moment.now()
                    });
                });
            } else {
                let targetDefaultModel = await ExtLinkedinTargetModel.findOne({
                    default_for_sync: true,
                    apply_to_sync: true,
                    niid: friendNiid
                }, function (err, target) {
                    if (err) return handleError(err);
                    return target;
                });
                let project_id = _.get(targetDefaultModel, 'project_id');
                let user_id = _.get(targetDefaultModel, 'created_by');
                let added = await addToProject({user_id, niid, project_id, linkedin_relationship_id});
                await LinkedinProfilesRelationshipModel.findByIdAndUpdate(linkedin_relationship_id, {
                    add_to_project: true,
                    sync: true,
                    updated_at: moment.now()
                },function (err, result) {
                    if (err){
                        console.log(err);
                        return false;
                    }
                    return result
                });
            }
        });

        await Promise.all(linkedinRelationshipsMap).then(() => {
            console.log('end process');
            process.exit(0);
        })
    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }

};

main();

const addToProject = async ({user_id, niid, project_id, linkedin_relationship_id}) => {
    try {
        let linkedinProfile = await LinkedinProfilesModel.findOne({
            niid: niid
        }, function (err, linkedinProfile) {
            if (err) return handleError(err);
            return linkedinProfile;
        });

        let publicIdentifier = _.get(linkedinProfile, 'publicIdentifier');

        let kanban = await KanbanModel.findOne({
            project_id: project_id,
            publicIdentifier: publicIdentifier,
        });

        if (!_.isEmpty(kanban)) {
            console.log('kanban đã tồn tại');
            return false;
        }

        let status = await ProjectStatusModel.findOne({
            project_id: project_id
        }, null, {sort: {display_order: -1}});

        let status_id = _.get(status,'_id');


        if(_.isEmpty(status_id)) {
            console.log('project khong ton tai trang thai');
            return false;
        }

        let last_kanban = await KanbanModel.findOne({
            status: status_id,
            project_id: project_id
        });
        let display_order = _.get(last_kanban, 'display_order', 0) + 1;

        console.log({
            project_id: project_id,
            group_id: _.get(project_id, 'group_id'),
            user_id: user_id,
            linkedin_relationship: linkedin_relationship_id,
            publicIdentifier: publicIdentifier,
            niid: niid,
            display_order: display_order,
            status: status_id
        });




        let kanban_id = await KanbanModel.create({
            project_id: project_id,
            group_id: _.get(project_id, 'group_id'),
            user_id: user_id,
            linkedin_relationship: linkedin_relationship_id,
            publicIdentifier: publicIdentifier,
            niid: niid,
            display_order: display_order,
            status: status_id
        }, function (err, kanban) {
            if (err) return handleError(err);

            return _.get(kanban, '_id')
        });

        let logActivity = await LogActivityModel.create({
            created_by: user_id,
            type: KANBAN_ADD_NEW,
            meta: {
                'kanban_id': kanban_id,
                'project_id': project_id,
                'group_id': _.get(project_id, 'group_id'),
                'publicIdentifier': publicIdentifier
            }
        }, function (err, logActivity) {
            if (err) return handleError(err);

            return _.get(logActivity, '_id')
        });
        return true;

    } catch (e) {
        console.log('add project faile');
        process.exit(1)
    }
};

const handleError = async (error) => {
    console.log(error);
    return false;
};

// add project
// mark relationship sync true, add_to_project true
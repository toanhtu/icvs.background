import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";
const {Schema} = mongoose;

const ProjectSchema = Schema({
    user_id : String,
    name : String,
    desc : String,
    group_id : String,
    status : String,
    member : Array,
    statuses : Array,
    created_at: {type: Date, default: moment.now()},
    updated_at:  {type: Date, default: moment.now()}
});
const ProjectModel = mongoose.model("projects", ProjectSchema, 'projects');

export default ProjectModel;

import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';
import moment from 'moment';

import _ from 'lodash';
import LinkedinProfilesModel from "../../../model/LinkedinProfilesModel";
import {transformData} from "../../Common/ProfileDataHelper";
import LinkedinProfileTransformedModel, {updateOrCreateLinkedinTransformed} from "../../../model/LinkedinProfileTransformedModel";
import {
    getBackgroundBatch,
    markBackgroundBatch,
    SYNC_LINKEDIN_PROFILE,
    PROCESSING
} from "../../../model/BackgroundBatchModel";

const main = async () => {
    try {

        let backgroundBatch = await getBackgroundBatch(SYNC_LINKEDIN_PROFILE);
        let limit = 200;
        let skip = 0;

        if (!_.isEmpty(backgroundBatch)) {
            console.log('run by background batch');
            limit = _.get(backgroundBatch, 'limit');
            skip = _.get(backgroundBatch, 'skip');
        }
        //danh dau


        let linkedinProfiles = await LinkedinProfilesModel
            .find({})
            .sort({
                _id: 1
            })
            .skip(skip)
            .limit(limit)
            .exec()
            .then(
                function (result, err) {
                    if (err) {
                        console.log(err);
                        process.exit(1);
                    }

                    return result
                });
        if (linkedinProfiles.length) {
            let markedID = await markBackgroundBatch({
                limit: limit,
                skip: skip + limit,
                status: PROCESSING,
                type: SYNC_LINKEDIN_PROFILE
            });
        }

        let linkedinProfileMap = _.map(linkedinProfiles, async (linkedinProfile) => {
            let niid = _.get(linkedinProfile, 'niid');
            console.log('niid', niid);
            let isDuplicate = await checkDuplicateNiid(niid);

            if (isDuplicate) {
                return null;
            }

            let publicIdentifier = _.get(linkedinProfile, 'publicIdentifier');
            let profileData = _.get(linkedinProfile, 'profileData');
            let included = _.get(profileData, 'included');
            if (!_.isEmpty(included)) {
                profileData = included;
            }

            let dataTransformed = transformData(profileData, niid);
            if(!dataTransformed) {
                console.log(niid,'not full profileData');
                return;
            }
            await updateOrCreateLinkedinTransformed(niid, dataTransformed)
        });

        await Promise.all(linkedinProfileMap).then(() => {
            console.log('done');
            process.exit(0);
        })
    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }
};

main();

const checkDuplicateNiid = async (niid) => {
    let linkedinTransformed = await LinkedinProfileTransformedModel.findOne({
            niid: niid,
            transformed_at: {
                '$gte': moment().subtract(15, 'days')
            }
        }
    );

    return !_.isEmpty(linkedinTransformed);
};


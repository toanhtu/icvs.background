import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";
const {Schema} = mongoose;

const ProjectStatusSchema = Schema({
    user_id : String,
    name : String,
    display_order : Number,
    project_id : String,
    color : String,
    created_at: {type: Date, default: moment.now()},
    updated_at:  {type: Date, default: moment.now()}
});
const ProjectStatusModel = mongoose.model("project_status", ProjectStatusSchema, 'project_status');

export default ProjectStatusModel;

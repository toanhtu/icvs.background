import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';
import moment from 'moment';

import _ from 'lodash';

import {
    CLASSIFIED,
    updateLinkedinProfilesRelationship,
    updateStatusLinkedinProfilesRelationship,
    getLinkedinProfileRelationshipForClassified,
    getLinkedinProfileRelationship
} from '../../../model/LinkedinProfilesRelationshipModel';

import {
    getLinkedinClassifyRequest, SUCCESS, updateLinkedinClassifyRequest
} from '../../../model/ExtLinkedinClassifyRequestModel';
import {
    getClassifiedTargetClassified,
    getTargetToClassified,
    markClassifiedTarget, updatePausedClassifiedTarget
} from '../../../model/ExtLinkedinTargetModel';

import {makeMatchingTargetProfile} from "./../../Common/ProfileDataHelper";
import {step_verify_profile} from "../../Common/TargetHelper";
import {parseDate} from "../../Common/DateTimeHelper";
import {getProfileData} from "../../../model/LinkedinProfilesModel";


const main = async () => {

    try {
        let done = false;
        console.log('begin');
        let linkedinClassifyRequests = await getLinkedinClassifyRequest();

        if (linkedinClassifyRequests.length === 0) {
            console.log('end process');
            process.exit(1);
        }
        /**
         * Lấy các yêu cầu kết nối
         */
        _.each(linkedinClassifyRequests, async (classifyRequest, indexClassifyRequest) => {

            let niid = _.get(classifyRequest, 'niid');
            console.log(niid);
            let max_num_connection = _.get(classifyRequest, 'num_connections');

            let max_linkedin_relationship = await getLinkedinProfileRelationship(niid);

            let targets = await getTargetToClassified(niid, Math.min(max_num_connection, max_linkedin_relationship));
            if (targets.length === 0) {
                let updateStatus = false;
                try {

                    let allTargets = await getClassifiedTargetClassified(niid);
                    let min_last_classified_amount = _.get(allTargets, '0.last_classified_amount');
                    if (min_last_classified_amount >= max_num_connection) {
                        updateStatus = updateLinkedinClassifyRequest(classifyRequest.id, {
                            status: SUCCESS,
                            re_sync: false
                        });
                        if (updateStatus && indexClassifyRequest === linkedinClassifyRequests.length - 1) {
                            console.log('end loop');
                            process.exit(0);
                        }
                    }

                    let targetNoPause = _.filter(allTargets, (target) => {
                        return !target.paused_at;
                    });

                    if (targetNoPause.length === 0) {
                        let updateStatus = updateLinkedinClassifyRequest(classifyRequest.id, {
                            status: SUCCESS,
                            re_sync: false
                        });
                        if (updateStatus && indexClassifyRequest === linkedinClassifyRequests.length - 1) {
                            console.log('tam dung dong bo');
                            process.exit(0);
                        }
                    }


                    if (min_last_classified_amount >= max_linkedin_relationship) {
                        //update những target có last >= total profile current

                        let updateTime = updateLinkedinClassifyRequest(classifyRequest.id, {classified_at: moment().toDate()});
                        if (updateTime && indexClassifyRequest === linkedinClassifyRequests.length - 1) {
                            console.log('tam dung dong bo');
                            process.exit(0);
                        }
                    }

                } catch (e) {
                    console.log('loi');
                    process.exit(1);
                }
            }

            /**
             * Match các đối tượng với profile đó
             */
            _.each(targets, async (target, indexTarget) => {
                let last_classified_amount = _.get(target, 'last_classified_amount', 0);

                let requestForClassifieds = await getLinkedinProfileRelationshipForClassified({
                    niid,
                    last_classified_amount,
                });

                if (!_.isEmpty(requestForClassifieds)) {

                    _.each(requestForClassifieds, async (requestForClassified, indexRequestForClassified) => {
                        let id = requestForClassified._id;
                        let niid = _.get(requestForClassified, 'niid');
                        let profile = _.get(requestForClassified, 'profile_meta');

                        if (_.isEmpty(profile)) {
                            let profileData = await getProfileData(niid);
                            if (_.isEmpty(profileData)) {
                                let updateStatus = await updateStatusLinkedinProfilesRelationship(id);
                                if (updateStatus) {
                                    console.log('update lai trang thai WAITING_FOR_FULLDATA');
                                    process.exit(0);
                                }
                            }

                            profile = makeMatchingTargetProfile(profileData);
                        }

                        let match = await step_verify_profile(profile, target);
                        let target_ids = _.get(requestForClassified, 'target_ids', []);

                        let dataUpdate = {
                            ...match ? {
                                target_ids: _.uniq([...target_ids, target._id.toString()])
                            } : {target_ids: _.uniq(target_ids)},
                            profile_meta: profile,
                            status: CLASSIFIED,
                            sync: false,
                            updated_at: moment.now()
                        };

                        await updateLinkedinProfilesRelationship(id, dataUpdate);

                        let done = false;
                        if (indexRequestForClassified === requestForClassifieds.length - 1) {
                            done = await markClassifiedTarget(target, {
                                last_classified_at: parseDate(_.get(requestForClassified, 'created_at')),
                                last_classified_niid: niid,
                                last_classified_amount: requestForClassifieds.length
                            });
                        }

                        if (indexClassifyRequest === linkedinClassifyRequests.length - 1 &&
                            indexTarget === targets.length - 1 &&
                            indexRequestForClassified === requestForClassifieds.length - 1 &&
                            done
                        ) {
                            console.log('end loop');
                            process.exit(0);
                        }

                    })
                } else {
                    let paused = await updatePausedClassifiedTarget({
                        niid: niid,
                        last_classified_amount: {
                            '$gte': max_linkedin_relationship
                        }
                    });

                    if (paused && indexTarget === targets.length - 1 && indexClassifyRequest === linkedinClassifyRequests.length - 1) {
                        console.log('stop');
                        process.exit(0);
                    }
                }

            });

        });
    }
    catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }

};

main();



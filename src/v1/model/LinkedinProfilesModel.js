import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";

const {Schema} = mongoose;

const LinkedinProfilesSchema = Schema({
    niid: {type: String},
    friendNiid: {type: String},
    status: {type: String},
    created_at: {type: Date},
    profileData: mongoose.Mixed,
    publicIdentifier: String
});
const LinkedinProfilesModel = mongoose.model("linkedin_profiles", LinkedinProfilesSchema, 'linkedin_profiles');


/**
 * Lấy ra profile đã đồng bộ data và chờ để phân loại
 * @param niid
 * @returns {Promise<T | string>}
 */
export async function getProfileData(niid) {

    return await LinkedinProfilesModel.aggregate([
        {
            $match: {
                niid: niid
            }
        }, {
            $project: {
                profileData: 1,
                publicIdentifier: 1
            }
        }, {
            $limit: 1
        }
    ])
        .exec()
        .then((profileDataResponse) => {

            const profileData = _.get(profileDataResponse, '0.profileData');

            if (!_.isEmpty(_.get(profileData, 'included'))) {
                return _.get(profileData, 'included');
            }
            if (_.isEmpty(profileData)) {
                console.log('profile loi', niid);
                process.exit(1);
                return;
            }
            return profileData;
        })
        .catch((err) => {
            console.log(err, 'error');
            return;
        });
}

export default LinkedinProfilesModel;


/**
 *
 * @param limit
 * @param skip
 */
export async function getlinkedProfileData(limit, skip) {
    return await LinkedinProfilesModel.find({})
        .sort({
            _id : 1
        })
        .skip(skip)
        .limit(limit)
        .exec()
        .then(
            function (result, err) {
                if (err) {
                    console.log(err);
                }

                return result
            });

}

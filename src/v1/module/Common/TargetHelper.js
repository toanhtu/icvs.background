import {
    MAX_ACURRACY_NUMBER_MATCHING_TITLE,
    MAX_ACURRACY_NUMBER_MATCHING_TITLE_AND_INDUSTRY
} from '../../config/constant';
import FuzzySet from 'fuzzyset.js';
import _ from 'lodash';

const fix_empty_jobtitle = (target) => {
    let job_title = _.filter(target.job_title, (jt) => {
        if (!_.isEmpty(jt)) {
            return jt;
        }
    });

    target.job_title = job_title;
}

export const step_verify_profile = async (profile, target) => {

    let locationTarget = target.location;
    let industryTarget = target.industry;
    let jobTitleTarget = target.job_title;
    let locationProfile = _.get(profile, "location");
    let industryProfile = _.get(profile, "industry", []);
    let jobTitleProfile = _.get(profile, "job_title", []);
    let matchLocation = false;
    let matchIndustry = false;
    let matchJobTitle = false;

    if (
        (locationTarget && locationTarget.length === 0) ||
        locationTarget.includes(locationProfile)
    ) {
        matchLocation = true;
    }

    if (
        (industryTarget && industryTarget.length === 0) ||
        (industryProfile && industryTarget.some(industry => industryProfile.includes(industry)))
    ) {
        matchIndustry = true;
    }

    if (
        (jobTitleTarget && jobTitleTarget.length === 0) ||
        (jobTitleProfile && jobTitleTarget.some(job_title => jobTitleProfile.includes(job_title)))
    ) {
        matchJobTitle = true;
    }

    if (
        jobTitleProfile.length > 0 && matchJobTitle == false
    ) {
        fix_empty_jobtitle(target);

        let fzz = FuzzySet(target.job_title);
        let stop = false;
        let acurracy = MAX_ACURRACY_NUMBER_MATCHING_TITLE;
        if (industryTarget.length > 0 && matchIndustry) {
            // Sử dụng trường hợp này nếu có cấu hình ít nhất 1 lĩnh vực nào đó khi khoanh vùng đối tượng
            // Trường hợp có cấu hình industry khoanh vùng thì độ chính xác cần cao hơn
            acurracy = MAX_ACURRACY_NUMBER_MATCHING_TITLE_AND_INDUSTRY;
        }
        _.each(jobTitleProfile, (jot) => {
            if (stop) {
                return;
            }
            const resp_fzz = fzz.get(jot);
            if (resp_fzz !== null) {
                const final_item = pickBetterAcurracyMatching(resp_fzz);
                if (final_item >= acurracy) {
                    // Any case is true then all is true
                    //console.log(`${jot} => `, resp_fzz);
                    matchJobTitle = true;
                    stop = true;
                }
            }
        });
    }

    return matchLocation && matchIndustry && matchJobTitle;
};

const pickBetterAcurracyMatching = (arr) => {
    let highitem = arr[0][0];

    _.each(arr, (item, ki) => {
        _.each(item, (child, kc) => {
            if (_.isNumber(arr[ki][kc])) {
                if (highitem < arr[ki][kc]) {
                    highitem = arr[ki][kc];
                }
            }

        })
    });

    return highitem;
}


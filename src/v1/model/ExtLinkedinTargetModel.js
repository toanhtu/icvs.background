import mongoose from 'mongoose';
import _ from 'lodash';
import moment from "moment";

const Schema = mongoose.Schema;


const ExtLinkedinTargetSchema = Schema({
    name: {type: String},
    job_title: {type: Array},
    location: {type: Array},
    industry: {type: Array},
    apply_to_sync: {type: Boolean},
    created_by: {type: String},
    niid: {type: String},
    active: {type: Boolean},
    last_classified_niid: String,
    last_classified_at: Date,
    paused_at: Date,
    last_classified_amount: Number,
    project_id: {type: String},
    updated_at: {type: Date},
    created_at: {type: Date},
    deleted_at: {type: Date},
});

const ExtLinkedinTargetModel = mongoose.model("ext_linkedin_target", ExtLinkedinTargetSchema, 'ext_linkedin_target');


export async function markClassifiedTarget(target, data) {

    let target_id = _.get(target, '_id');

    try {
        await ExtLinkedinTargetModel.updateOne({_id: new mongoose.Types.ObjectId(target_id)}, {
            $set: {
                last_classified_at: data.last_classified_at,
                last_classified_niid: data.last_classified_niid
            },
            $inc: {
                last_classified_amount: data.last_classified_amount,
            }
        });
        return true;
    } catch (error) {
        console.log(error);
        process.exit(1);
        return false;
    }

}

/**
 * Lấy ra đối tượng mà user đang cần phân loại
 * @returns {Promise<T | string>}
 */
export async function getTargetClassified() {

    return await ExtLinkedinTargetModel.find({
        apply_to_sync: true,
        active: true,
        default_for_sync: {
            '$ne': true
        },
        deleted_at: {
            '$exists': false
        },
        '$or': [
            {
                done: false,
            },
            {
                done: {
                    '$exists': false
                }
            }
        ]

    },).limit(300)
        .exec()
        .then((targets) => {
            return targets;
        })
        .catch((err) => {
            return 'error occured' + JSON.stringify(err);
        });
}


/**
 * Lấy ra đối tượng mà user đang cần phân loại
 * @param niid
 * @param max_num_connection
 * @returns {Promise<T | string>}
 */
export async function getTargetToClassified(niid, max_num_connection) {

    return await ExtLinkedinTargetModel.find({
        $and: [
            {
                niid: niid,
                apply_to_sync: true,
                active: true,
                default_for_sync: {
                    '$ne': true
                },
                deleted_at: {
                    '$exists': false
                }
            },
            {
                '$or': [
                    {
                        last_classified_amount: {
                            '$lt': max_num_connection
                        },
                    },
                    {
                        last_classified_amount: {
                            '$exists': false
                        }
                    },
                ],
            },
            {
                '$or': [
                    {
                        paused_at: {
                            '$lt': moment().subtract(1, 'hour')
                        },
                    },
                    {
                        paused_at: {
                            '$exists': false
                        }
                    },
                ],
            }
        ]
    })
        .exec()
        .then((targets) => {
            return targets;
        })
        .catch((err) => {
            return 'error occured' + JSON.stringify(err);
        });
}


/**
 * Lấy min value
 * @param niid
 * @returns {Promise<T | string>}
 */
export async function getClassifiedTargetClassified(niid) {

    return await ExtLinkedinTargetModel.find({
        $and: [
            {
                niid: niid,
                apply_to_sync: true,
                active: true,
                default_for_sync: {
                    '$ne': true
                },
                deleted_at: {
                    '$exists': false
                }
            },
        ]
    }).sort({
        last_classified_amount: 1
    })
        .exec()
        .then((targets) => {
            return targets;
        })
        .catch((err) => {
            console.log('error occured' + JSON.stringify(err));
        });
}

/**
 * pause target
 * @param filter
 */
export async function updatePausedClassifiedTarget(filter) {
    try {
        let done = await ExtLinkedinTargetModel.updateMany(filter, {paused_at: moment()});

        return true;
    } catch (e) {
        return false;
    }
}

/**
 * Lấy ra đối tượng mà user đang cần phân loại
 * @param niid
 */
export async function getMinTargetClassified(niid) {

    return await ExtLinkedinTargetModel.find({
        niid: niid,
        apply_to_sync: true,
        default_for_sync: {
            '$exists': false
        }
    })
        .sort({last_classified_amount: 1})
        .limit(1)
        .exec()
        .then(targets => targets[0].last_classified_amount);
}


/**
 * lấy ra các target theo niid
 * @param niid
 */
export async function getJobTitleTarget(niid) {
    return await ExtLinkedinTargetModel.find({
        niid: niid,
        active: true,
        })
        .sort({
            _id: -1
        })
        .limit(100)
        .exec()
        .then((jobTitleData) => {
            return jobTitleData;
        }).catch((err) => {
            console.log(err)
        })
}


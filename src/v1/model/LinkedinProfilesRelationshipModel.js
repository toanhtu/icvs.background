import mongoose from 'mongoose';
import {PROCESSING, SUCCESS} from "./ExtLinkedinClassifyRequestModel";
import moment from "moment";
const Schema = mongoose.Schema;

export const WAITING_FOR_FULLDATA = "WAITING_FOR_FULLDATA";
export const WAITING_FOR_CLASSIFY = "WAITING_FOR_CLASSIFY";
export const CLASSIFIED = "CLASSIFIED";

const LinkedinProfilesRelationshipSchema = Schema({
    _id: Object,
    niid: String,
    friendNiid: String,
    status: String,
    created_at: Date,
    updated_at: Date,
    target_ids: Array,
    profile_meta: Object,
    add_to_project: Boolean,
    sync: Boolean,
    classified: Boolean
});

const LinkedinProfilesRelationshipModel = mongoose.model("linkedin_profiles_relationship", LinkedinProfilesRelationshipSchema, 'linkedin_profiles_relationship');


/**
 * Lấy request đồng bộ và phân loại đang PROCESSING
 * @param id
 * @param data
 */
export async function updateLinkedinProfilesRelationship(id, data) {

    try {
        await LinkedinProfilesRelationshipModel.updateOne({_id: new mongoose.Types.ObjectId(id)}, {$set: data});

        return;
    } catch (error) {
        console.log(error);
        process.exit(1);
        return false;
    }
}


/**
 *
 * @param friendNiid
 * @param minClassified
 * @returns {Promise<boolean>}
 */
export async function markLinkedinProfilesRelationship({friendNiid, minClassified}) {

    try {
        let ids = await LinkedinProfilesRelationshipModel.find({
            status: CLASSIFIED,
            friendNiid: friendNiid,
            // classified: false
        })
            .sort({_id: 1})
            .limit(minClassified)
            .distinct('_id');

        ids = ids.map(id => {
            return new mongoose.Types.ObjectId(id)
        });
        let result = await  LinkedinProfilesRelationshipModel.updateMany({_id: {$in: ids}}, {
            classified: true
        });
        console.log(result);
        return true;
    } catch (error) {
        console.log(error);
        process.exit(1);
        return false;
    }
}


export async function updateStatusLinkedinProfilesRelationship(id) {

    try {

        await LinkedinProfilesRelationshipModel.updateOne({_id: new mongoose.Types.ObjectId(id)}, {
            $set: {
                status: WAITING_FOR_FULLDATA
            }
        });
        return true;

    } catch (error) {
        console.log(error);
        process.exit(1);
        return false;
    }

}

/**
 * Lấy ra profile đã đồng bộ data và chờ để phân loại
 * @param niid
 * @param last_classified_at
 * @param last_classified_amount
 * @returns {Promise<T | string>}
 */
export async function getLinkedinProfileRelationshipForClassified({niid, last_classified_amount}) {

    return await LinkedinProfilesRelationshipModel.find({
        status: {
            '$in': [WAITING_FOR_CLASSIFY, CLASSIFIED]
        },
        friendNiid: niid,
    })
        .sort({
            _id: 1
        })
        .skip(last_classified_amount)
        .limit(300)
        .exec()
        .then((profile) => {
            return profile;
        })
        .catch((err) => {
            console.log(err);
            return 'error occured' + JSON.stringify(err);
        });
}


/**
 * Lấy ra profile đã đồng bộ data và chờ để phân loại
 * @returns {Promise<T | string>}
 */
export async function getLinkdedinRelationShipToAddProject() {

    return await LinkedinProfilesRelationshipModel.find({
        status: CLASSIFIED,
        add_to_project: false,
        classified: true
    })
        .sort({
            _id: 1
        })
        .limit(300)
        .exec()
        .then((profile) => {
            return profile;
        })
        .catch((err) => {
            console.log(err);
            return 'error occured' + JSON.stringify(err);
        });
}



/**
 * Lấy ra profile đã đồng bộ data và chờ để phân loại
 * @param niid
 */
export async function getLinkedinProfileRelationship(niid) {

    return await LinkedinProfilesRelationshipModel.countDocuments({
        friendNiid: niid,
    })
        .then((count) => {
            return count;
        })
        .catch((err) => {
            console.log(err);
            return 'error occured' + JSON.stringify(err);
        });
}

/**
 * lấy ra niid để check bạn bè
 * @param niid
 */
export  async function geLinkedinProfileRelationShipAutoGenConnect(niid) {
    return await LinkedinProfilesRelationshipModel.find({
        niid : niid
    })
        .sort({
            _id: -1
        })
        .limit(100)
}
export default LinkedinProfilesRelationshipModel;
import mongoose from 'mongoose';
import _ from "lodash";
import moment from "moment";

const {Schema} = mongoose;

const KanbanSchema = Schema({
    project_id : String,
    user_id : String,
    niid : String ,
    publicIdentifier: String,
    linkedin_relationship: String,
    display_order : Number,
    status : String,
    created_at: {type: Date, default: moment.now()},
    updated_at:  {type: Date, default: moment.now()}
});
const KanbanModel = mongoose.model("kanban", KanbanSchema, 'kanban');

export default KanbanModel;

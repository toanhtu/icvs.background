import dotenv from 'dotenv/config';
import openConnection from '../../../database/database';

import _ from 'lodash';

import {
    CLASSIFIED,
} from '../../../model/LinkedinProfilesRelationshipModel';

import {SUCCESS, SUCCESS_SYNC} from '../../../model/ExtLinkedinClassifyRequestModel';
import ExtLinkedinClassifyRequestModel from "../../../model/ExtLinkedinClassifyRequestModel";
import moment from "moment";
import LinkedinProfilesRelationshipModel from "../../../model/LinkedinProfilesRelationshipModel";


const main = async () => {

    try {
        let requestClassifieds = ExtLinkedinClassifyRequestModel.find({
            status: SUCCESS,
            '$or': [
                {
                    check_success_at: {
                        '$exists': false
                    }
                },
                {
                    check_success_at: {
                        '$lte': moment().subtract(1, 'minutes').toDate()
                    }
                },

            ]
        })
            .sort({'created_at': 1})
            .limit(300)
            .exec()
            .then((request) => {
                return request;
            })
            .catch((err) => {
                console.log(err);
                return false;
            });

        let requestClassifiedMap = _.map(requestClassifieds, async (requestClassified) => {
            let request_classified_id = _.get(requestClassified, '_id');
            let num_connections = _.get(requestClassified, 'num_connections');
            let niid = _.get(requestClassified, 'niid');

            let linkedinclassifiedTotal = await LinkedinProfilesRelationshipModel.countDocuments({
                status: CLASSIFIED,
                sync: true,
                friendNiid: niid
            });
            console.log(linkedinclassifiedTotal, num_connections);
            if (linkedinclassifiedTotal >= num_connections) {
                console.log('đã phân loại xong');
                await ExtLinkedinClassifyRequestModel.findByIdAndUpdate(request_classified_id, {
                    status: SUCCESS_SYNC,
                }, function (err, result) {
                    if (err) {
                        console.log(err);
                        return false;
                    }
                    return result
                });
            } else {
                console.log("1 phut sau check lại");
                await ExtLinkedinClassifyRequestModel.findByIdAndUpdate(request_classified_id, {
                    check_success_at: moment.now(),
                });
            }
        });
        await Promise.all(requestClassifiedMap ).then(() => {
            process.exit(0);
        })
    } catch (e) {
        if (e) {
            console.log('error', e);
            process.exit(1);
        }
    }

};

main();


// là từng target đã đồng bộ xong
// update status SUCCESS_SYNC
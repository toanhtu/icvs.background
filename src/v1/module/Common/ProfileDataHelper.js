import _ from 'lodash';
import moment from 'moment';
import {decodeHtmlEntity} from "./HtmlHelper";

export const getFullProfileEntities = (profile_data) => {
    let profile = null;
    _.each(profile_data, (prof) => {
        if (_.includes(prof['recipeTypes'], 'com.linkedin.voyager.dash.deco.identity.profile.FullProfileWithEntities')) {
            profile = prof;
            return;
        }
    });
    return profile;
};

export const makeMatchingTargetProfile = (profile_data) => {

    const industries = getIndustriesProfile(profile_data);
    const [locations, titles] = getJobtitleProfile(profile_data);

    const single_profile = getFullProfileEntities(profile_data);

    if(_.isEmpty(single_profile)) {
        return;
    }

    let headline = false;
    if ( typeof single_profile['headline'] != 'undefined' && _.includes(titles, single_profile['headline']) == false) {
        titles.push(single_profile['headline']);
        headline = single_profile['headline'];
    }

    let location = 'Unknown';

    if (typeof single_profile['locationName'] != ' undefined ') {
        location = _.trim(single_profile['locationName']);
    }

    return {
        location: location,
        industry: (industries.length > 0) ? industries : false,
        job_title: (titles.length > 0) ? titles : false,
        headline: headline
    };

};

export const getJobtitleProfile = (profile_data) => {
    let titles = [];
    let locations = [];
    let company_ids = [];

    _.each(profile_data, (inc) => {
        if (inc['type'] == 'com.linkedin.voyager.dash.organization.Company') {
            if (typeof inc['entityUrn'] != 'undefined') {
                if (_.includes(company_ids, inc['entityUrn']) == false) {
                    company_ids.push(inc['entityUrn']);
                }
            }
        }
    });


    _.each(profile_data, (inc) => {
        if (inc['type'] == 'com.linkedin.voyager.dash.identity.profile.Position') {
            if (typeof inc['companyUrn'] !== 'undefined' && typeof inc['title'] !== 'undefined') {
                if (_.includes(company_ids, inc['companyUrn'])) {

                    titles.push(decodeHtmlEntity(inc['title']));
                } else if (typeof inc['title'] !== 'undefined') {
                    titles.push(decodeHtmlEntity(inc['title']));
                }

                if (typeof inc['locationName'] !== 'undefined') {
                    if (_.includes(locations, inc[`locationName`]) == false) {
                        locations.push(inc[`locationName`]);
                    }
                }
            }
        }
    });

    return [locations, titles];
}

/**
 * array profile_data
 * @param profile_data
 */
export const transformData = (profile_data) => {

    let fullProfile = getFullProfileEntities(profile_data);

    if (_.isEmpty(fullProfile)) {
        console.log('empty full profile');
        return false;
    }
    fullProfile = _.pick(fullProfile,[
        "firstName",
        "lastName",
        "fullName",
        "publicIdentifier",
        "headline",
        "pictureUrl",
        "summary",
        "locationName",
        "address",
    ]);

    let educations = getEducation(profile_data);
    let companies = getCompany(profile_data);
    let skills = getSkill(profile_data);
    let firstName = _.get(fullProfile, 'firstName');
    let lastName = _.get(fullProfile, 'lastName');
    let fullName = firstName + ' ' + lastName;
    let profile_meta = makeMatchingTargetProfile(profile_data);

    return {
        niid,
        fullProfile,
        fullName,
        ...fullProfile,
        ...educations ? {educations} : {},
        ...companies ? {companies} : {},
        ...skills ? {skills} : {},
        profile_meta,
        transformed_at: moment.now()
    }
};


const getSkill = (profile_data) => {
    return _.filter(profile_data, (inc) => {
        if (inc[`type`] === 'com.linkedin.voyager.dash.organization.Skill') {
            return inc;
        }
    })
};
const getEducation = (profile_data) => {
    return _.filter(profile_data, (inc) => {
        if (inc[`type`] === 'com.linkedin.voyager.dash.organization.School') {
            return inc;
        }
    })
};

const getCompany = (profile_data) => {
    return _.filter(profile_data, (inc) => {
        if (inc[`type`] === 'com.linkedin.voyager.dash.organization.Company') {
            return inc;
        }
    })
};
/**
 * array profile_data
 * @param profile_data
 */
export const transformData = (profile_data, niid) => {
    let fullProfile = getFullProfileEntities(profile_data);
    if (_.isEmpty(fullProfile)) {
        console.log('empty full profile');
        return false;
    }
    fullProfile = _.pick(fullProfile,[
        "firstName",
        "lastName",
        "fullName",
        "publicIdentifier",
        "headline",
        "pictureUrl",
        "summary",
        "locationName",
        "address",
    ]);

    let educations = getEducation(profile_data);
    let companies = getCompany(profile_data);
    let skills = getSkill(profile_data);
    let firstName = _.get(fullProfile, 'firstName');
    let lastName = _.get(fullProfile, 'lastName');
    let fullName = firstName + ' ' + lastName;
    let profile_meta = makeMatchingTargetProfile(profile_data);

    return {
        niid,
        fullName,
        ...fullProfile,
        ...educations ? {educations} : {},
        ...companies ? {companies} : {},
        ...skills ? {skills} : {},
        profile_meta,
        transformed_at: moment.now()
    }
};


const getSkill = (profile_data) => {
    return _.filter(profile_data, (inc) => {
        if (inc[`type`] === 'com.linkedin.voyager.dash.organization.Skill') {
            return inc;
        }
    })
};
const getEducation = (profile_data) => {
    return _.filter(profile_data, (inc) => {
        if (inc[`type`] === 'com.linkedin.voyager.dash.organization.School') {
            return inc;
        }
    })
};

const getCompany = (profile_data) => {
    return _.filter(profile_data, (inc) => {
        if (inc[`type`] === 'com.linkedin.voyager.dash.organization.Company') {
            return inc;
        }
    })
};

export const getIndustriesProfile = (profile_data) => {
    let industry_ids = [];

    _.each(profile_data, (inc) => {
        if (inc[`type`] == 'com.linkedin.voyager.dash.organization.Company') {
            if (typeof inc[`industry`] !== 'undefined' && typeof inc[`name`] !== 'undefined') {
                _.each(inc[`industry`], (industry, key) => {
                    if (_.includes(industry_ids, industry) == false) {
                        industry_ids.push(industry);
                    }
                });
            }
        }
    });

    const single_profile = getFullProfileEntities(profile_data);

    if (single_profile == null) {
        return [];
    }
    let industry_profile_urn;
    let industry_profile_text;
    if (typeof single_profile['industry'] !== 'undefined') {
        industry_profile_urn = single_profile['industry'];
    } else if (typeof single_profile['industryUrn'] !== 'undefined') {
        industry_profile_urn = single_profile['industryUrn'];
    }

    let industry_text = [];
    if (industry_ids.length > 0) {
        _.each(profile_data, (inc) => {
            if (inc['type'] == 'com.linkedin.voyager.dash.common.Industry') {
                const text = decodeHtmlEntity(inc['name']);
                if (typeof inc['entityUrn'] !== 'undefined') {
                    if (_.includes(industry_ids, inc['entityUrn'])) {
                        if (_.includes(industry_ids, inc['entityUrn'])) {
                            if (_.includes(industry_text, text) == false) {
                                industry_text.push(text);
                            }
                        }

                        if (inc['entityUrn'] == industry_profile_urn) {
                            industry_profile_text = text;
                        }

                    }
                }
            }
        });
    }


    if (_.includes(industry_text, industry_profile_text) == false) {
        if (_.trim(industry_profile_text)) {
            industry_text.push(industry_profile_text);
        }
    }
    return industry_text;
}


import mongoose from 'mongoose';

const Schema = mongoose.Schema;

const UserSettingSchema = mongoose.Schema({
    _id: Object,
    user_id: String,
    niid: String,
    package_id:String,
    auto_connect_service: mongoose.Mixed,
});


const UserSettingModel = mongoose.model('user_setting', UserSettingSchema, 'user_setting')

export default UserSettingModel;




// lấy ra những user đang có trạng thái online
/**
 *
 * @param skip
 * @param limit
 */
export  async function getUserSettingData(skip, limit) {
    return  await UserSettingModel.find({
        'auto_connect_service.on': true,
    })
        .sort({_id:-1})
        .limit(limit)
        .skip(skip)
}

